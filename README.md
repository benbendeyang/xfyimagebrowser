# XFYImageBrowser

[![CI Status](https://img.shields.io/travis/leonazhu/XFYImageBrowser.svg?style=flat)](https://travis-ci.org/leonazhu/XFYImageBrowser)
[![Version](https://img.shields.io/cocoapods/v/XFYImageBrowser.svg?style=flat)](https://cocoapods.org/pods/XFYImageBrowser)
[![License](https://img.shields.io/cocoapods/l/XFYImageBrowser.svg?style=flat)](https://cocoapods.org/pods/XFYImageBrowser)
[![Platform](https://img.shields.io/cocoapods/p/XFYImageBrowser.svg?style=flat)](https://cocoapods.org/pods/XFYImageBrowser)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYImageBrowser is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYImageBrowser'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYImageBrowser is available under the MIT license. See the LICENSE file for more info.
