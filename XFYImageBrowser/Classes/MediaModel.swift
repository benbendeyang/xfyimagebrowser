//
//  MediaModel.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/7/26.
//

import Foundation

// MARK: - 资源用途类型
public enum MediaType {
    
    case picture
    case video
}
public struct MediaModel {
    var url: String?
    var placeholder: UIImage?
    var type: MediaType = .picture
    public init(url: String?, placeholder: UIImage?, type: MediaType = .picture) {
        self.url = url
        self.placeholder = placeholder
        self.type = type
    }
}
