//
//  MediaBrowserController.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/7/26.
//
//  媒体预览

open class MediaBrowserController: UIViewController {
    
    let dismissInteractive = DismissTransitionInteractive()
    
    private lazy var mainCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        layout.itemSize = UIScreen.main.bounds.size
        let collectionView = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
        collectionView.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.register(ImageBrowserCell.self, forCellWithReuseIdentifier: "ImageBrowserCell")
        collectionView.register(VideoBrowserCell.self, forCellWithReuseIdentifier: "VideoBrowserCell")
        return collectionView
    }()
    private lazy var backgroundTap: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(touchBackground))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        return tap
    }()
    private lazy var pageControl: UIPageControl = {
        let ScreenWidth = UIScreen.main.bounds.width
        let ScreenHeight = UIScreen.main.bounds.height
        let control = UIPageControl(frame: CGRect(x: 0, y: ScreenHeight - 30, width: ScreenWidth, height: 12))
        control.backgroundColor = .clear
        return control
    }()
    
    private var mediaModels: [MediaModel] = [] {
        didSet {
            refreshPageControl()
            mainCollectionView.reloadData()
        }
    }
    private var initIndex: Int?
    
    public init(mediaModels: [MediaModel], selectIndex: Int? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.mediaModels = mediaModels
        self.initIndex = selectIndex
        self.refreshPageControl()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open var prefersStatusBarHidden: Bool {
        return true
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initView()
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let currentCell = mainCollectionView.cellForItem(at: IndexPath(row: pageControl.currentPage, section: 0)) as? VideoBrowserCell {
            currentCell.playerViewController.player?.play()
        }
    }
}

// MARK: - 私有方法
private extension MediaBrowserController {
    
    func initView() {
        dismissInteractive.controller = self
        transitioningDelegate = self
        view.addSubview(mainCollectionView)
        view.addSubview(pageControl)
        view.addGestureRecognizer(backgroundTap)
        if let index = initIndex, index < mediaModels.count {
            pageControl.currentPage = index
            let offX = UIScreen.main.bounds.width * CGFloat(index)
            mainCollectionView.setContentOffset(CGPoint(x: offX, y: 0), animated: false)
            initIndex = nil
        }
    }
    
    @objc func touchBackground() {
        dismiss(animated: true)
    }
    
    func refreshPageControl() {
        if mediaModels.count > 1 {
            pageControl.numberOfPages = mediaModels.count
            pageControl.currentPage = 0
            pageControl.isHidden = false
        } else {
            pageControl.isHidden = true
        }
    }
    
    func pageChange(fromIndex: Int, toIndex: Int) {
        if let toCell = mainCollectionView.cellForItem(at: IndexPath(row: toIndex, section: 0)) as? VideoBrowserCell {
            toCell.playerViewController.player?.play()
        }
        if let fromCell = mainCollectionView.cellForItem(at: IndexPath(row: fromIndex, section: 0)) as? VideoBrowserCell {
            fromCell.playerViewController.player?.pause()
        }
    }
}

// MARK: - 协议
extension MediaBrowserController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaModels.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let mediaModel = mediaModels[indexPath.row]
        switch mediaModel.type {
        case .picture:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageBrowserCell", for: indexPath) as! ImageBrowserCell
            cell.show(model: ImageBrowserModel(url: mediaModel.url, placeholder: mediaModel.placeholder))
            backgroundTap.require(toFail: cell.doubleTap)
            return cell
        case .video:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoBrowserCell", for: indexPath) as! VideoBrowserCell
            cell.show(model: mediaModel)
            return cell
        }
    }
}

extension MediaBrowserController: UIScrollViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
        guard 0 <= page, page < mediaModels.count  else { return }
        if pageControl.currentPage != page {
            pageChange(fromIndex: pageControl.currentPage, toIndex: page)
            pageControl.currentPage = page
        }
    }
}

// MARK: 转场
extension MediaBrowserController: UIViewControllerAnimatedTransitioning {
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let entranceType: TransitionEntranceType = (transitionContext.viewController(forKey: .to) == self) ? .appear : .dismiss
        let duration = transitionDuration(using: transitionContext)
        switch entranceType {
        case .appear:
            guard let toView = transitionContext.view(forKey: .to) else { return }
            toView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            transitionContext.containerView.addSubview(toView)
            UIView.animate(withDuration: duration, animations: {
                toView.frame = UIScreen.main.bounds
            }) { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        case .dismiss:
            guard let toView = transitionContext.view(forKey: .to) else { return }
            guard let fromView = transitionContext.view(forKey: .from) else { return }
            toView.frame = UIScreen.main.bounds
            fromView.frame = UIScreen.main.bounds
            transitionContext.containerView.addSubview(toView)
            transitionContext.containerView.addSubview(fromView)
            UIView.animate(withDuration: duration, animations: {
                fromView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            }) { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        }
    }
}

extension MediaBrowserController: UIViewControllerTransitioningDelegate {
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    public func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return dismissInteractive.isInteracting ? dismissInteractive : nil
    }
}

// MARK: - 交互转场
enum TransitionEntranceType {
    case appear
    case dismiss
}
class DismissTransitionInteractive: UIPercentDrivenInteractiveTransition {
    
    lazy var panGesture: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
    weak var controller: UIViewController? {
        didSet {
            controller?.view.addGestureRecognizer(panGesture)
        }
    }
    weak var transitionContext: UIViewControllerContextTransitioning?
    var isInteracting = false
    
    override func startInteractiveTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        super.startInteractiveTransition(transitionContext)
        self.transitionContext = transitionContext
    }
    
    @objc func handlePan(gesture: UIPanGestureRecognizer) {
        
        func finishOrCancel() {
            (percent > 0.4) ? finish() : cancel()
        }
        
        switch gesture.state {
        case .began:
            isInteracting = true
            controller?.dismiss(animated: true)
        case .changed:
            guard isInteracting else { return }
            update(percent)
        case .cancelled:
            guard isInteracting else { return }
            finishOrCancel()
            isInteracting = false
        case .ended:
            guard isInteracting else { return }
            finishOrCancel()
            isInteracting = false
        default:
            cancel()
            isInteracting = false
        }
    }
    
    var percent: CGFloat {
        guard let transitionContainerView = transitionContext?.containerView else { return 0 }
        let translation = panGesture.translation(in: transitionContainerView)
        let height = transitionContainerView.bounds.height
        return max(0, translation.y/height * 1.8)
    }
}
