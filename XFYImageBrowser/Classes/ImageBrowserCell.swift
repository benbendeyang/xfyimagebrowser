//
//  ImageBrowserCell.swift
//  Pods-XFYImageBrowser_Example
//
//  Created by 🐑 on 2019/3/20.
//

import UIKit
import Kingfisher

public struct ImageBrowserModel {
    var url: String?
    var placeholder: UIImage?
    public init(url: String?, placeholder: UIImage?) {
        self.url = url
        self.placeholder = placeholder
    }
}

open class ImageBrowserCell: UICollectionViewCell {

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 3
        scrollView.delegate = self
        return scrollView
    }()
    public lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    public private(set) lazy var doubleTap: UITapGestureRecognizer = {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(tapDoubleDid))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        return doubleTap
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        scrollView.frame = bounds
        addSubview(scrollView)
        
        imageView.frame = bounds
        scrollView.addSubview(imageView)
        
        imageView.addGestureRecognizer(doubleTap)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        resetSize()
    }
    
    @objc func tapDoubleDid() {
        UIView.animate(withDuration: 0.3, animations: {
            if self.scrollView.zoomScale == self.scrollView.minimumZoomScale {
                self.scrollView.zoomScale = self.scrollView.maximumZoomScale
            } else {
                self.scrollView.zoomScale = self.scrollView.minimumZoomScale
            }
        })
    }
    
    open func show(model: ImageBrowserModel) {
        if let url = model.url {
            imageView.kf.setImage(with: URL(string: url), placeholder: model.placeholder) { [weak self] _ in
                self?.resetSize()
            }
        } else {
            imageView.image = model.placeholder
            resetSize()
        }
    }
    
    private func resetSize() {
        scrollView.frame = contentView.bounds
        scrollView.zoomScale = 1.0
        if let image = imageView.image {
            imageView.frame.size = scaleSize(size: image.size)
            imageView.center = scrollView.center
        }
    }
    
    private func scaleSize(size: CGSize) -> CGSize {
        let width = size.width
        let height = size.height
        let widthRatio = width/UIScreen.main.bounds.width
        let heightRatio = height/UIScreen.main.bounds.height
        let ratio = max(heightRatio, widthRatio)
        return CGSize(width: width/ratio, height: height/ratio)
    }
}

// MARK: - UIScrollViewDelegate
extension ImageBrowserCell: UIScrollViewDelegate {

    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    public func scrollViewDidZoom(_ scrollView: UIScrollView) {
        var centerX = scrollView.center.x
        var centerY = scrollView.center.y
        centerX = (scrollView.contentSize.width > scrollView.frame.size.width) ? scrollView.contentSize.width/2 : centerX
        centerY = (scrollView.contentSize.height > scrollView.frame.size.height) ? scrollView.contentSize.height/2 : centerY
        imageView.center = CGPoint(x: centerX, y: centerY)
    }
}
