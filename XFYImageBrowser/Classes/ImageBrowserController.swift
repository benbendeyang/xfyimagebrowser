//
//  ImageBrowserController.swift
//  Pods-XFYImageBrowser_Example
//
//  Created by 🐑 on 2019/3/19.
//

import UIKit

open class ImageBrowserController: UIViewController {
    
    private lazy var mainCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        layout.itemSize = UIScreen.main.bounds.size
        let collectionView = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
        collectionView.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.register(ImageBrowserCell.self, forCellWithReuseIdentifier: "ImageBrowserCell")
        return collectionView
    }()
    private lazy var backgroundTap: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(touchBackground))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        return tap
    }()
    private lazy var pageControl: UIPageControl = {
        let ScreenWidth = UIScreen.main.bounds.width
        let ScreenHeight = UIScreen.main.bounds.height
        let control = UIPageControl(frame: CGRect(x: 0, y: ScreenHeight - 30, width: ScreenWidth, height: 12))
        control.backgroundColor = .clear
        return control
    }()

    private var imageModels: [ImageBrowserModel] = [] {
        didSet {
            refreshPageControl()
            mainCollectionView.reloadData()
        }
    }
    private var initIndex: Int?
    
    public init(imageModels: [ImageBrowserModel], selectIndex: Int? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.imageModels = imageModels
        self.initIndex = selectIndex
        self.refreshPageControl()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open var prefersStatusBarHidden: Bool {
        return true
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
}

// MARK: - 私有方法
private extension ImageBrowserController {
    
    func initView() {
        view.addSubview(mainCollectionView)
        view.addSubview(pageControl)
        view.addGestureRecognizer(backgroundTap)
        if let index = initIndex, index < imageModels.count {
            pageControl.currentPage = index
            let offX = UIScreen.main.bounds.width * CGFloat(index)
            mainCollectionView.setContentOffset(CGPoint(x: offX, y: 0), animated: false)
            initIndex = nil
        }
    }
    
    @objc func touchBackground() {
        dismiss(animated: true)
    }
    
    func refreshPageControl() {
        if imageModels.count > 1 {
            pageControl.numberOfPages = imageModels.count
            pageControl.currentPage = 0
            pageControl.isHidden = false
        } else {
            pageControl.isHidden = true
        }
    }
}

// MARK: - 协议
extension ImageBrowserController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageModels.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageBrowserCell", for: indexPath) as! ImageBrowserCell
        cell.show(model: imageModels[indexPath.row])
        backgroundTap.require(toFail: cell.doubleTap)
        return cell
    }
}

extension ImageBrowserController: UIScrollViewDelegate {
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.bounds.width)
        pageControl.currentPage = page
    }
}
