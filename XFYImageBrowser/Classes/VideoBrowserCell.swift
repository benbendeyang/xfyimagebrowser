//
//  VideoBrowserCell.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/7/26.
//
//  视频单元

import AVKit

open class VideoBrowserCell: UICollectionViewCell {
    
    let playerViewController = AVPlayerViewController()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        playerViewController.view.frame = CGRect(x: 0, y: 44, width: bounds.width, height: bounds.height - 44 * 2)
        playerViewController.view.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1)
        addSubview(playerViewController.view)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func show(model: MediaModel) {
        guard let url = model.url, let videoUrl = URL(string: url) else { return }
        let playerItem = AVPlayerItem(url: videoUrl)
        let player = AVPlayer(playerItem: playerItem)
        player.rate = 1.0
        playerViewController.player = player
        player.pause()
    }
}
