//
//  UIViewController+ImageBrowserExtension.swift
//  Kingfisher
//
//  Created by 🐑 on 2019/3/20.
//

import Foundation

public extension UIViewController {
    
    func presentImageBrowser(imageModels: [ImageBrowserModel], selectIndex: Int? = nil) {
        let imageBrowserController = ImageBrowserController(imageModels: imageModels, selectIndex: selectIndex)
        present(imageBrowserController, animated: true)
    }
    func presentImageBrowser(imageUrls: [String], placeholder: UIImage? = nil, selectIndex: Int? = nil) {
        let imageModels = imageUrls.compactMap { url -> ImageBrowserModel in
            return ImageBrowserModel(url: url, placeholder: placeholder)
        }
        presentImageBrowser(imageModels: imageModels, selectIndex: selectIndex)
    }
}
