//
//  ViewController.swift
//  XFYImageBrowser
//
//  Created by leonazhu on 03/19/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit
import XFYImageBrowser
import AVKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func browse1(_ sender: Any) {
        let imageModels: [ImageBrowserModel] = [
            ImageBrowserModel(url: "http://b-ssl.duitang.com/uploads/item/201510/04/20151004080802_nTJYz.jpeg", placeholder: #imageLiteral(resourceName: "default")),
            ImageBrowserModel(url: "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg", placeholder: #imageLiteral(resourceName: "default")),
            ImageBrowserModel(url: "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg", placeholder: #imageLiteral(resourceName: "default"))
        ]
        presentImageBrowser(imageModels: imageModels)
    }
    
    @IBAction func browse2(_ sender: Any) {
        let imageModels: [ImageBrowserModel] = [
            ImageBrowserModel(url: "http://b-ssl.duitang.com/uploads/item/201510/04/20151004080802_nTJYz.jpeg", placeholder: #imageLiteral(resourceName: "default")),
            ImageBrowserModel(url: "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg", placeholder: #imageLiteral(resourceName: "default")),
            ImageBrowserModel(url: "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg", placeholder: #imageLiteral(resourceName: "default"))
        ]
        presentImageBrowser(imageModels: imageModels, selectIndex: 1)
    }
    
    @IBAction func browse3(_ sender: Any) {
        let imageUrls: [String] = [
            "http://b-ssl.duitang.com/uploads/item/201510/04/20151004080802_nTJYz.jpeg",
            "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg",
            "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg"
        ]
        presentImageBrowser(imageUrls: imageUrls)
    }
    
    @IBAction func mediaBrowse(_ sender: Any) {
        var mediaModels: [MediaModel] = []
        let mediaMedel1 = MediaModel(url: "https://test-ledong-house-1258261725.cos.ap-guangzhou.myqcloud.com/old_house/video/D288A1F7-4C9C-44E7-89C3-29A788E9891A%20(1).mov", placeholder: #imageLiteral(resourceName: "default"), type: .video)
        let mediaMedel2 = MediaModel(url: "http://b-ssl.duitang.com/uploads/item/201510/04/20151004080802_nTJYz.jpeg", placeholder: #imageLiteral(resourceName: "default"))
        let mediaMedel3 = MediaModel(url: "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg", placeholder: #imageLiteral(resourceName: "default"))
        let mediaMedel4 = MediaModel(url: "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg", placeholder: #imageLiteral(resourceName: "default"))
        let mediaMedel5 = MediaModel(url: "https://test-ledong-house-1258261725.cos.ap-guangzhou.myqcloud.com/old_house/video/1550039076615_920120.mp4", placeholder: #imageLiteral(resourceName: "default"), type: .video)
        mediaModels.append(mediaMedel1)
        mediaModels.append(mediaMedel2)
        mediaModels.append(mediaMedel3)
        mediaModels.append(mediaMedel4)
        mediaModels.append(mediaMedel5)
        let mediaBrowserController = MediaBrowserController(mediaModels: mediaModels, selectIndex: 4)
        present(mediaBrowserController, animated: true)
    }
}
